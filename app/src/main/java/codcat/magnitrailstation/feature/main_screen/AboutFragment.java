package codcat.magnitrailstation.feature.main_screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import codcat.magnitrailstation.R;
import codcat.magnitrailstation.di.scopes.ActivityScope;

@ActivityScope
public class AboutFragment extends DaggerFragment{

    @Inject MainScreenMvp.Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_fragment, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Inject
    public AboutFragment() {
        // Requires empty public constructor for @ContributesAndroidInjector
    }
}
