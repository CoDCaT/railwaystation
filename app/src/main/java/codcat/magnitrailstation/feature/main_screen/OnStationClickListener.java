package codcat.magnitrailstation.feature.main_screen;

import codcat.magnitrailstation.data.pojo.Station;

public interface OnStationClickListener {
    void onStationClick(Station station);
}
